package name.alexkosarev.tutorials

import org.springframework.data.mongodb.core.ReactiveMongoOperations
import org.springframework.data.mongodb.core.query.Criteria.where
import org.springframework.data.mongodb.core.query.Query.query
import org.springframework.data.mongodb.core.query.Update.update
import org.springframework.http.MediaType
import org.springframework.http.codec.ServerSentEvent
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Flux
import reactor.core.publisher.FluxSink
import reactor.core.publisher.Mono
import reactor.core.publisher.switchIfEmpty
import java.util.*

@Component
class TodoHandler(private val mongoOperations: ReactiveMongoOperations) {

    private val eventEmitters: MutableList<FluxSink<ServerSentEvent<*>>> = Collections.synchronizedList(mutableListOf())

    private fun publishEvent(event: String, todoId: UUID) = this.eventEmitters.forEach {
        it.next(ServerSentEvent.builder(mapOf("event" to event, "todoId" to todoId)).build())
    }

    fun getTodoList(serverRequest: ServerRequest): Mono<ServerResponse> =
            ServerResponse.ok().contentType(MediaType.APPLICATION_JSON_UTF8)
                    .body(this.mongoOperations.findAll(Todo::class.java), Todo::class.java)

    fun getTodoById(serverRequest: ServerRequest): Mono<ServerResponse> =
            Mono.justOrEmpty(serverRequest.pathVariables()["todoId"])
                    .map { UUID.fromString(it) }
                    .map { this.mongoOperations.findOne(query(where("_id").`is`(it)), Todo::class.java) }
                    .flatMap { ServerResponse.ok().body(it, Todo::class.java) }
                    .switchIfEmpty { ServerResponse.notFound().build() }

    fun createTodo(serverRequest: ServerRequest): Mono<ServerResponse> =
            Mono.justOrEmpty(serverRequest.queryParam("task"))
                    .filter { it.isNotBlank() }
                    .map { Todo(task = it) }
                    .flatMap { todo ->
                        ServerResponse.ok().body(this.mongoOperations.save(todo)
                                .doOnNext { this.publishEvent("TodoCreated", it.id) }, Todo::class.java)
                    }
                    .switchIfEmpty { ServerResponse.badRequest().build() }

    fun modifyTodo(serverRequest: ServerRequest): Mono<ServerResponse> =
            Mono.justOrEmpty(serverRequest.pathVariables()["todoId"])
                    .map { UUID.fromString(it) }
                    .flatMap { id ->
                        Mono.justOrEmpty(serverRequest.queryParam("task"))
                                .filter { it.isNotBlank() }
                                .flatMap { task ->
                                    this.mongoOperations.updateFirst(query(where("_id").`is`(id)),
                                            update("task", task), Todo::class.java)
                                }
                                .filter { it.wasAcknowledged() && it.matchedCount == 1L && it.modifiedCount == 1L }
                                .doOnNext { this.publishEvent("TodoModified", id) }
                    }
                    .flatMap { ServerResponse.noContent().build() }
                    .switchIfEmpty { ServerResponse.notFound().build() }

    fun markTodoAsCompleted(serverRequest: ServerRequest): Mono<ServerResponse> =
            Mono.justOrEmpty(serverRequest.pathVariables()["todoId"])
                    .map { UUID.fromString(it) }
                    .flatMap { id ->
                        this.mongoOperations.updateFirst(query(where("_id").`is`(id)), update("completed", true),
                                Todo::class.java)
                                .filter { it.wasAcknowledged() && it.matchedCount == 1L && it.modifiedCount == 1L }
                                .doOnNext { this.publishEvent("TodoMarkedAsCompleted", id) }
                    }
                    .flatMap { ServerResponse.noContent().build() }
                    .switchIfEmpty { ServerResponse.notFound().build() }

    fun markTodoAsNotCompleted(serverRequest: ServerRequest): Mono<ServerResponse> =
            Mono.justOrEmpty(serverRequest.pathVariables()["todoId"])
                    .map { UUID.fromString(it) }
                    .flatMap { id ->
                        this.mongoOperations.updateFirst(query(where("_id").`is`(id)), update("completed", false),
                                Todo::class.java)
                                .filter { it.wasAcknowledged() && it.matchedCount == 1L && it.modifiedCount == 1L }
                                .doOnNext { this.publishEvent("TodoMarkedAsNotCompleted", id) }
                    }
                    .flatMap { ServerResponse.noContent().build() }
                    .switchIfEmpty { ServerResponse.notFound().build() }

    fun deleteTodo(serverRequest: ServerRequest): Mono<ServerResponse> =
            Mono.justOrEmpty(serverRequest.pathVariables()["todoId"])
                    .map { UUID.fromString(it) }
                    .flatMap { id ->
                        this.mongoOperations.remove(query(where("_id").`is`(id)), Todo::class.java)
                                .filter { it.wasAcknowledged() && it.deletedCount == 1L }
                                .doOnNext { this.publishEvent("TodoDeleted", id) }
                    }
                    .flatMap { ServerResponse.noContent().build() }
                    .switchIfEmpty { ServerResponse.notFound().build() }

    fun subscribeOnEvents(serverRequest: ServerRequest): Mono<ServerResponse> =
            ServerResponse.ok()
                    .contentType(MediaType.APPLICATION_STREAM_JSON)
                    .body(Flux.create {
                        this.eventEmitters.add(it)
                        it.onDispose { this.eventEmitters.remove(it) }
                        it.onCancel { this.eventEmitters.remove(it) }
                    }, ServerSentEvent::class.java)
}
