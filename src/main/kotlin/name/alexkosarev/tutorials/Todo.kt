package name.alexkosarev.tutorials

import java.util.*

data class Todo(val id: UUID = UUID.randomUUID(),
                val completed: Boolean = false,
                val task: String)
